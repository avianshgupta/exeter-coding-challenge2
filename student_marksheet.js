const fastify = require('fastify')({
    logger: true
});

var students = [
		{
		"StudentName": "student1",
		"StudentID": "s1",
		"subject1": 100,
		"subject2": 98,
		"subject3": 76,
		"subject4": 82,
		"subject5": 92
		},
		{
		"StudentName": "student2",
		"StudentID": "s2",
		"subject1": 92,
		"subject2": 100,
		"subject3": 98,
		"subject4": 76,
		"subject5": 82
		},
		{
		"StudentName": "student3",
		"StudentID": "s3",
		"subject1": 76,
		"subject2": 92,
		"subject3": 100,
		"subject4": 98,
		"subject5": 76
		},
		{
		"StudentName": "student4",
		"StudentID": "s4",
		"subject1": 82,
		"subject2": 76,
		"subject3": 92,
		"subject4": 100,
		"subject5": 98
		}
	]

// GET /report
fastify.get('/report', async (req, res) => {
    res.send(students);
});

// POST /add
fastify.post('/add', async (req, res) => {
    const stud  = req.body;
	let flag = 0;
	let keys = Object.keys(students[0]);
	let keys_stud = Object.keys(stud);
	if (keys.length === keys_stud.length){
		for (let i = 0; i < keys.length; i++){
			if (keys[i] != keys_stud[i]){
				flag = 1;
				break;
			}
			else if (typeof stud[keys_stud[i]] != typeof students[0][keys[i]]){
				flag = 2;
				break
			}
			else if ((typeof stud[keys_stud[i]] === 'number') && !(0 <= stud[keys_stud[i]] && stud[keys_stud[i]] <= 100)){
				flag = 3;
			}
		}
	}
	else{
		flag = 4;
	}

	if (!flag) {
        students.push(stud);
    	res.send(stud);
    } else {
		var message = "";
		switch (flag){
			case 1: message = "Invalid Key";
					break;
			case 2: message = "Invalid datatype";
					break;
			case 3: message = "Marks should be between 0 to 100 inclusive"
					break;
			case 4: message = "Invalid number of keys"
					break;
		}
        res.code(400).send({"message": message});
    }
	
});

// POST /update
fastify.post('/update/:Id', async (req, res) => {
    let stud = req.body;
    let flag = 3;
	let keys = Object.keys(students[0]);
	let keys_stud = Object.keys(stud);
	let ind = 0;
    for (let i = 0; i < students.length; i++) {   
		if (students[i].StudentID === req.params.Id) {
			for(let j = 0; j < keys_stud.length; j++){
				if (!keys.includes(keys_stud[j])){
					flag = 1;
					break;
				}
				else if ((typeof stud[keys_stud[j]] === 'number') && !(0 <= stud[keys_stud[j]] && stud[keys_stud[j]] <= 100)){
					flag = 2;
					break;
				}
			}
			if (flag == 3){
				keys_stud.forEach(key => {
					students[i][key] = stud[key]
				});
				ind = i
				flag = 0;
				break;
			}
		} 
    }

    if (!flag) {
        res.send(students[ind]); 
    } else {
        switch (flag){
			case 1: message = "Invalid Key";
					break;
			case 2: message = "Marks should be between 0 to 100 inclusive"
					break;
			case 3: message = "Student with given id not found";
					break;
		}
        res.code(400).send({"message": message});
    }
});

// POST /delete
fastify.delete('/delete/:Id', async (req, res) => {
    let stud;
    let flag = false;

    for (let i = 0; i < students.length; i++) {        
		if (students[i].StudentID == req.params.Id) {
			stud = students[i];
			students.splice(i, 1);
			flag = true;
			break;
		} 
    }

    if (flag) {
        res.send(stud); 
    } else {
        res.code(404).send({"message": "Student with given id not found"});
    }
});



const startServer = async () => { 
    try {
        await fastify.listen(5000);
    } catch (err) {
        fastify.log.error(err);
    }
};

startServer();
